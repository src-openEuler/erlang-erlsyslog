%global realname erlsyslog
%global upstream lemenkov
Name:		erlang-%{realname}
Version:	0.8.0
Release:	4
Summary:	Syslog facility for Erlang
License:	MIT
URL:		https://github.com/%{upstream}/%{realname}
Source0:	https://github.com/%{upstream}/%{realname}/archive/%{version}/%{realname}-%{version}.tar.gz
BuildRequires:  erlang-rebar gcc
BuildRequires:  erlang-eunit
%description
Syslog facility for Erlang.

%prep
%setup -q -n %{realname}-%{version}

%build
%{erlang_compile}

%install
%{erlang_install}

%check
%{erlang_test}

%files
%doc example
%{erlang_appdir}/

%changelog
* Tue Jun 04 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 0.8.0-4
- Remove useless patches

* Sun Jul 23 2023 yaoxin <yao_xin001@hoperun.com> - 0.8.0-3
- Fix test failure

* Mon May  8 2023 Wenlong Zhang <zhangwenlong@loongson.cn> - 0.8.0-2
- fix build error for loongarch64

* Sat Sep 19 2020 huanghaitao <huanghaitao8@huawei.com> - 0.8.0-1
- package init
